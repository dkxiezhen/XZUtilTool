import XCTest
@testable import XZUtilTool

final class XZUtilToolTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(XZUtilTool().text, "Hello, World!")
    }
}
